# Backend Developer tech assignment - JukeBox

## Supported REST API - '/jukebox'

A REST API "/jukebox" with a single GET endpoint which returns a paginated list of jukeboxes that suppport a given setting id. It supports following query parameters:

 - `settingId` - setting id (required)
 - `model` - filter by model name (optional)
 - `offset` - specifies at what index start the page (optional)
 - `limit` - specifies the page size (optional)

### Sample URI(S):
 **GET** `http://<SERVER_IP>:8080/jukebox?setting=<settingId>`

 **GET** `http://<SERVER_IP>:8080/jukebox?setting=<settingId>&model=<modelName>&offset=<offset>&limit=<limit>`

 Sample Requests and output are provided for reference at the end of this ReadMe.

## Intrinsic Features to this sample application
 - Sample Javadocs
 - Application logging using log4j
 - Read the mockRestApi url from application property file
 - Error Handling using thymeleaf
 - Unit Tests implemented with JUnit Framework
 - Swagger API
 - Dockerized image
 - CI/CD implementaion with the use of Gitlab CI pipeline

 More details follow below.

### Swagger API
 **API Specification** `http://localhost:8080/v3/api-docs/`

 **API Docs** `http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config`

### CI/CD - Using Gitlab CI pipeline
 **CI/CD pipeline** `https://gitlab.com/backend-test/jukebox/-/pipelines`

![](GitlabCI_Pipeline.png)

Since SERVER specified via variable is not available, the current pipeline has deploy stage **Failed** marked with **X**

**Latest pipeline (with correct Server_IP) has all stages in _SUCCESS_ and marked with _√_**

 This pipeline has below stages
 - Unittest --> Run the unit tests
 - Build --> Package the application as jar using 'mvn package'
 - Package --> Dockerize the app and pushes the image to Gitlab Container Registry
 - Deploy --> Deploy the app to the VM machine (specified by variable: SERVER)

 The pipeline is completely automated and triggers on every commit in the repository.

 Docker image is created and pushed to Gitlab Container Registry @ 'https://gitlab.com/backend-test/jukebox/container_registry'

 The application will be deployed on the VM machine specified in the Gitlab CI variables:
 - SERVER: SERVER_IP address
 - SSH_PRIVATE_KEY: Private Key used to login on SERVER

 New pipeline can be triggered by providing new values for these variables via 'https://gitlab.com/backend-test/jukebox/-/pipelines/new'


## Maven Build - using mvnw wrapper
 - Build and run the application: .\mvnw spring-boot:run

 - Clean generated build files: .\mvnw clean

 - Clean build of application: .\mvnw clean package
 
 - Run the app using generated JAR file: java -jar target/jukebox-springboot-0.0.1-SNAPSHOT.jar


## Sample Error page
 User will be redirected to error page, in case of any errors

 Example - If settingId requested in the RESTAPI request is invalid, 404 is returned

 **GET** `http://localhost:8080/jukebox?setting=123`

![](Error404.png)
 

## Sample Request & Output:
 **GET** `http://localhost:8080/jukebox?setting=9ac2d388-0f1b-4137-8415-02b953dd76f7`

 ```js
 [{
     "id": "5ca94a8acfdeb5e01e5bdbe8",
		"model": "virtuo",
		"components": [
			{
				"name": "money_storage"
			},
			{
				"name": "money_pcb"
			},
			{
				"name": "money_storage"
			},
			{
				"name": "camera"
			},
			{
				"name": "money_receiver"
			}
		]
 }]
```

  **GET** `http://localhost:8080/jukebox?setting=207797de-5857-4c60-a69b-80eea28bcce8&model=angelina`

 ```js
 [{
     "id": "5ca94a8a77e20d15a7d16d0a",
		"model": "angelina",
		"components": [
			{
				"name": "pcb"
			},
			{
				"name": "money_pcb"
			},
			{
				"name": "touchscreen"
			},
			{
				"name": "speaker"
			},
			{
				"name": "speaker"
			}
		]
	},
	{
		"id": "5ca94a8a8b58770bb38055a0",
		"model": "angelina",
		"components": [
			{
				"name": "money_storage"
			},
			{
				"name": "pcb"
			}
		]
	},
	{
		"id": "5ca94a8af9985926172d6e8d",
		"model": "angelina",
		"components": [
			{
				"name": "speaker"
			},
			{
				"name": "led_matrix"
			},
			{
				"name": "money_pcb"
			},
			{
				"name": "pcb"
			}
		]
 }]
```

   **GET** `http://localhost:8080/jukebox?setting=207797de-5857-4c60-a69b-80eea28bcce8&model=angelina&limit=2`

 ```js
 [{
     "id": "5ca94a8a77e20d15a7d16d0a",
		"model": "angelina",
		"components": [
			{
				"name": "pcb"
			},
			{
				"name": "money_pcb"
			},
			{
				"name": "touchscreen"
			},
			{
				"name": "speaker"
			},
			{
				"name": "speaker"
			}
		]
	},
	{
		"id": "5ca94a8a8b58770bb38055a0",
		"model": "angelina",
		"components": [
			{
				"name": "money_storage"
			},
			{
				"name": "pcb"
			}
		]
 }]
```

   **GET** `http://localhost:8080/jukebox?setting=207797de-5857-4c60-a69b-80eea28bcce8&model=angelina&offset=2`

 ```js
 [{
     "id": "5ca94a8af9985926172d6e8d",
		"model": "angelina",
		"components": [
			{
				"name": "speaker"
			},
			{
				"name": "led_matrix"
			},
			{
				"name": "money_pcb"
			},
			{
				"name": "pcb"
			}
		]
 }]
```

   **GET** `http://localhost:8080/jukebox?setting=207797de-5857-4c60-a69b-80eea28bcce8&model=angelina&offset=1&limit=1`

 ```js
 [{
     "id": "5ca94a8a8b58770bb38055a0",
		"model": "angelina",
		"components": [
			{
				"name": "money_storage"
			},
			{
				"name": "pcb"
			}
		]
 }]
 ```

## Feedback

Any feedback about this sample application can be provided via email <charumittal84@gmail.com>
