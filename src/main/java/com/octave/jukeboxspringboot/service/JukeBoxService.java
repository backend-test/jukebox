package com.octave.jukeboxspringboot.service;

import java.util.List;

import com.octave.jukeboxspringboot.model.JukeBox;

public interface JukeBoxService {

	/**
	 * Fetches the JukeBoxes which have settings compatible to the given settingID
	 * The JukeBoxes returned will have all the components required for the specific setting
	 * 
	 * If 6 JukeBoxe(s) are compatible with the specific setting and offset is specified as 2,
	 * it will return the JukeBox(s) starting from 3rd JukeBox in the list
	 * 
	 * @param settingId, setting id 
	 * @param modelName,  filter by model name 
	 * @param offset, specifies at what index start the page 
	 * @param limit, specifies the page size 
	 * @return List<JukeBox>, list of jukeBox compatible with setting
	 */
	List<JukeBox> getJukeBox(String settingId, String modelName, int offset, int limit);

}