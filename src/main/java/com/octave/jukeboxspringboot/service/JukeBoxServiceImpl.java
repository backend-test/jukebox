package com.octave.jukeboxspringboot.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.octave.jukeboxspringboot.dao.JukeBoxDao;
import com.octave.jukeboxspringboot.exception.ServiceException;
import com.octave.jukeboxspringboot.model.JukeBox;
import com.octave.jukeboxspringboot.model.JukeBoxComponent;
import com.octave.jukeboxspringboot.model.Setting;

@Service
public class JukeBoxServiceImpl implements JukeBoxService {
	private final static Logger logger = LogManager.getLogger(JukeBoxServiceImpl.class);
	
	@Autowired
	private JukeBoxDao jukeBoxDao;

	/**
	 * Fetches the JukeBoxes which have settings compatible to the given settingID
	 * The JukeBoxes returned will have all the components required for the specific setting
	 * 
	 * If 6 JukeBoxe(s) are compatible with the specific setting and offset is specified as 2,
	 * it will return the JukeBox(s) starting from 3rd JukeBox in the list
	 * 
	 * @param settingId, setting id 
	 * @param modelName,  filter by model name 
	 * @param offset, specifies at what index start the page 
	 * @param limit, specifies the page size 
	 * @return List<JukeBox>, list of jukeBox compatible with setting
	 */
	@Override
	public List<JukeBox> getJukeBox(String settingId,
			String modelName,
			int offset,
			int limit) {
		logger.info("Arguments::settingId=>" + settingId + "modelName=>" + modelName + "offset=>" + offset + "limit=>" + limit);

		Setting setting = jukeBoxDao.fetchSetting(settingId); 
		if(setting == null) {
			logger.error("Invalid Setting Id::" + settingId);
			throw new ServiceException("Invalid Setting Id");
		}
		JukeBoxComponent[] settingComps = setting.getComponents();
		
		List<JukeBox> jukes = jukeBoxDao.fetchAllJukeBox();
		
		List<JukeBox> jukeboxList = jukes.stream()
				.filter(juke -> {
					List<JukeBoxComponent> jc = Arrays.asList(juke.getComponents());
					List<JukeBoxComponent> sc = Arrays.asList(settingComps);
		
					boolean allcomp = true;
					if (jc.containsAll(sc)) {
						for (JukeBoxComponent s : sc) {
							if (Collections.frequency(sc, s) > Collections.frequency(jc, s)) {
								allcomp = false;
								break;
							}
						}
					} else {
						allcomp = false;
					}
					return allcomp;
				})
				.collect(Collectors.toList());

		jukeboxList = modelName.isEmpty() ? jukeboxList : filterDataForModelName(jukeboxList, modelName);

		jukeboxList = paginateData(jukeboxList, offset, limit);
		
		return jukeboxList;
	}
	
	/**
	 * Filter the JukeBox based on modelName
	 * 
	 * @param jukeBoxList, jukeBox list to be filtered
	 * @param modelName
	 * @return
	 */
	private  List<JukeBox> filterDataForModelName(List<JukeBox> jukeBoxList, String modelName){
		logger.debug("Arguments::modelName=>" + modelName);
		return jukeBoxList.stream().filter(p -> p.getModel().equals(modelName)).collect(Collectors.toList());
	}
	
	/**
	 * Returns paginated JukeBox list.
	 * Offset defines the starting elemnet from list. Offset=0 will return the elements from start. 
	 * Offset=1 will return the elements from 2
	 * Limit defines pagesize and it is the maximum number of jukebox to be returned 
	 * 
	 * @param jukeBoxList, jukeBox list to be filtered
	 * @param offset, the starting element for the jukeBox list
	 * @param limit, maximum number of jukebox to be shown
	 * @return
	 */
	private List<JukeBox> paginateData(List<JukeBox> jukeBoxList, int offset, int limit) {
		logger.debug("Arguments::offset=>" + offset + "limit=>" + limit);
		
		int size = jukeBoxList.size();
		
		//Return empty list if offset is greater than size OR limit is not 0
		if(offset >= size || limit == 0) {
			return Collections.emptyList();
		}
		
		offset = offset < 0 ? 0 : offset;
		
		limit = (limit < 0 || size < offset + limit) ? size : (offset + limit);
			
		return jukeBoxList.subList(offset, limit);
	}
	
}
