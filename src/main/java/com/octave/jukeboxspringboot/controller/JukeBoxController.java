package com.octave.jukeboxspringboot.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.octave.jukeboxspringboot.model.JukeBox;
import com.octave.jukeboxspringboot.service.JukeBoxService;

@RestController
public class JukeBoxController {

	private final static Logger logger = LogManager.getLogger(JukeBoxController.class);

	@Autowired
	private JukeBoxService jukeBoxService;

	/**
	 * Fetches the JukeBoxes which have settings compatible to the given settingID
	 * The JukeBoxes returned will have all the components required for the specific setting
	 * 
	 * If 6 JukeBoxe(s) are compatible with the specific setting and offset is specified as 2,
	 * it will return the JukeBox(s) starting from 3rd JukeBox in the list
	 * 
	 * @param settingId, setting id (required)
	 * @param modelName,  filter by model name (optional)
	 * @param offset, specifies at what index start the page (optional)
	 * @param limit, specifies the page size (optional)
	 * @return List<JukeBox>, list of jukeBox compatible with setting
	 */
	@GetMapping("/jukebox")
	public List<JukeBox> getJukeBox(@RequestParam(value = "setting") String settingId,
			@RequestParam(value = "model", defaultValue = "") String modelName,
			@RequestParam(value = "offset", defaultValue = "-1") int offset,
			@RequestParam(value = "limit", defaultValue = "-1") int limit) {

		logger.info("Arguments::settingId=>" + settingId + "model=>" + modelName + "offset=>" + offset + "limit=>" + limit);

		return jukeBoxService.getJukeBox(settingId, modelName, offset, limit);
	}

}
