package com.octave.jukeboxspringboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ServiceException extends RuntimeException {
	
	public ServiceException() {
		super();
	}
	
	public ServiceException(String exMsg) {
		super(exMsg);
	}
	
	public ServiceException(String exMsg, Throwable cause) {
        super(exMsg, cause);
    }
	
	public ServiceException(Throwable cause) {
        super(cause);
    }

}
