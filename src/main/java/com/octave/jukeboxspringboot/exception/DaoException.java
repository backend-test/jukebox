package com.octave.jukeboxspringboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class DaoException extends RuntimeException {
	
	public DaoException() {
		super();
	}
	
	public DaoException(String exMsg) {
		super(exMsg);
	}
	
	public DaoException(String exMsg, Throwable cause) {
        super(exMsg, cause);
    }
	
	public DaoException(Throwable cause) {
        super(cause);
    }

}
