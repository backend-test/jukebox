package com.octave.jukeboxspringboot.dao;

import java.util.Collection;
import java.util.List;

import com.octave.jukeboxspringboot.model.JukeBox;
import com.octave.jukeboxspringboot.model.Setting;

public interface JukeBoxDao {

	/**
	 * Fetches the setting for settingID
	 * 
	 * @param settingId
	 */
	Setting fetchSetting(String settingId);

	/**
	 * Fetches all the available settings
	 */
	Collection<Setting> fetchAllSetting();

	/**
	 * Fetches all the available JukeBoxes
	 */
	List<JukeBox> fetchAllJukeBox();

}