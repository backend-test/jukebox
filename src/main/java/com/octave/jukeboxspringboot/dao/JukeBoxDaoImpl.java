package com.octave.jukeboxspringboot.dao;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.octave.jukeboxspringboot.exception.DaoException;
import com.octave.jukeboxspringboot.model.JukeBox;
import com.octave.jukeboxspringboot.model.JukeBoxComponent;
import com.octave.jukeboxspringboot.model.Setting;

@Repository
public class JukeBoxDaoImpl implements JukeBoxDao {

	private final static Logger logger = LogManager.getLogger(JukeBoxDaoImpl.class);
	
	private static String URI_JUKEBOX;
	
	private static String URI_SETTING;
	
	private static List<JukeBox> jukes;
	private static Map<String, Setting> settings;
	
	public JukeBoxDaoImpl(@Value("${rest.url.jukebox}") String uri_juke, @Value("${rest.url.setting}") String uri_setting) throws DaoException {
		URI_JUKEBOX = uri_juke;
		URI_SETTING = uri_setting;
		
		initRepository();
	}
	
	/**
	 * Initializes the repo with all the available settings and JukeBoxes, since we are using the mockRestAPI
	 * to avoid recurrent calls to the same  mockRestAPI
	 * 
	 * In production this approach should not be used and only the required data to be fetched as and when required.
	 * If data is static than cache with aging can be used
	 */
	private void initRepository() throws DaoException {
		initSetting();
		initJukeBox();
	}
	
	/**
	 * Fetches the setting for settingID
	 * 
	 * @param settingId
	 */
	@Override
	public Setting fetchSetting(String settingId) {
		logger.info("Arguments::settingId=>" + settingId);
		return settings.get(settingId);
	}
	
	/**
	 * Fetches all the available settings
	 */
	@Override
	public Collection<Setting> fetchAllSetting() {
		logger.info("Entry::fetchAllSetting");
		return settings.values();
	}
	
	/**
	 * Fetches all the available JukeBoxes
	 */
	@Override
	public List<JukeBox> fetchAllJukeBox(){
		logger.info("Entry::fetchAllJukeBox");
		return jukes;
	}

	/**
	 * Initializes "jukes" with the list of all the available JukeBoxes
	 */
	private void initJukeBox() throws DaoException {
		logger.debug("Entry::initJukeBox");
		String response = getResponse(URI_JUKEBOX);

		// Using the JSON simple library parse the string into a json object
		JSONParser parse = new JSONParser();
		JSONArray jukelist = null;
		try {
			jukelist = (JSONArray) parse.parse(response);
		} catch (ParseException e) {
			logger.error("Exception caught::" + e.getMessage());
			e.printStackTrace();
			
			throw new DaoException("Error while parsing the response");
		}
		


		jukes = new ArrayList<JukeBox>(jukelist.size());
		logger.info("******************Juke******************");

		for (int i = 0; i < jukelist.size(); i++) {

			JSONObject juke = (JSONObject) jukelist.get(i);

			// Get the required object from the above created object
			String id = (String) juke.get("id");

			// Get the required object from the above created object
			String model = (String) juke.get("model");

			JSONArray components = (JSONArray) juke.get("components");

			JukeBoxComponent[] jukeBoxComponents = new JukeBoxComponent[components.size()];
			for (int j = 0; j < components.size(); j++) {
				JSONObject component = (JSONObject) components.get(j);

				// Get the required object from the above created object
				String componentName = (String) component.get("name");

				jukeBoxComponents[j] = new JukeBoxComponent(componentName);

			}
			JukeBox jukebox = new JukeBox(id, model, jukeBoxComponents);

			jukes.add(jukebox);
			logger.info("" + jukebox);
		}
	}

	/**
	 * Initializes "settings" with the list of all the available Settings
	 */
	private void initSetting() throws DaoException {
		logger.debug("Entry::initSetting");
		
		String response = getResponse(URI_SETTING);

		// Using the JSON simple library parse the string into a json object
		JSONParser parse = new JSONParser();
		JSONObject responseData = null;
		try {
			responseData = (JSONObject) parse.parse(response);			
		} catch (ParseException e) {
			logger.error("Exception caught::" + e.getMessage());
			e.printStackTrace();
			
			throw new DaoException("Error while parsing the response");
		}
		
		JSONArray settinglist = (JSONArray) responseData.get("settings");

		settings = new HashMap<String, Setting>(settinglist.size());
		logger.info("******************Settings******************");
		for (int i = 0; i < settinglist.size(); i++) {

			JSONObject setting = (JSONObject) settinglist.get(i);

			// Get the required object from the above created object
			String id = (String) setting.get("id");

			JSONArray components = (JSONArray) setting.get("requires");

			JukeBoxComponent[] settingComponents = new JukeBoxComponent[components.size()];
			for (int j = 0; j < components.size(); j++) {

				String componentName = (String) components.get(j);

				settingComponents[j] = new JukeBoxComponent(componentName);

			}

			settings.put(id, new Setting(id, settingComponents));

			logger.info("" + settings.get(id));
		}
	}

	/**
	 * Fetches the data from the specified restAPI of GET type only
	 *  
	 * @param requestURI
	 * @return response
	 * @throws DaoException
	 */
	private static String getResponse(String requestURI) throws DaoException {
		logger.debug("Arguments::requestURI=>" + requestURI);
		
		String inline = "";
		try {
			URL url = new URL(requestURI);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.connect();

			// Getting the response code
			int responsecode = conn.getResponseCode();

			if (responsecode != 200) {
				throw new RuntimeException("HttpResponseCode: " + responsecode);
			} else {

				Scanner scanner = new Scanner(url.openStream());

				// Write all the JSON data into a string using a scanner
				while (scanner.hasNext()) {
					inline += scanner.nextLine();
				}

				// Close the scanner
				scanner.close();
			}
		} catch (IOException e) {
			logger.error("Exception caught::" + e.getMessage());
			e.printStackTrace();
			
			throw new DaoException("Error while fetching the response from REST API");
		}

		return inline;
	}
}
