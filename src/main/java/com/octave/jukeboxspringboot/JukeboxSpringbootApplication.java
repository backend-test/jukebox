package com.octave.jukeboxspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JukeboxSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(JukeboxSpringbootApplication.class, args);
	}

}
