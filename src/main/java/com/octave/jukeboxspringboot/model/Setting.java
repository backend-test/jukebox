package com.octave.jukeboxspringboot.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Setting {
	private String id;
	private JukeBoxComponent[] components;
	
	public Setting(String id, JukeBoxComponent[] components) {
		this.id = id;
		this.components = components;
	}

	public String getId() {
		return id;
	}

	public JukeBoxComponent[] getComponents() {
		return components;
	}

	@Override
	public String toString() {
		return "Setting [id=" + id + ", components=" + Arrays.toString(components) + "]";
	}

}
