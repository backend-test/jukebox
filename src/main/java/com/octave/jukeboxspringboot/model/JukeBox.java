package com.octave.jukeboxspringboot.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class JukeBox {
	private String id;
	private String model;
//	private Map<JukeBoxComponent, Integer> components;
	private JukeBoxComponent[] components;
	
	public JukeBox(String id, String model, JukeBoxComponent[] components) {
		this.id = id;
		this.model = model;
		this.components = components;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public JukeBoxComponent[] getComponents() {
		return components;
	}
	public void setComponents(JukeBoxComponent[] components) {
		this.components = components;
	}

	@Override
	public String toString() {
		return "JukeBox [id=" + id + ", model=" + model + ", components=" + Arrays.toString(components) + "]";
	}

	
}
