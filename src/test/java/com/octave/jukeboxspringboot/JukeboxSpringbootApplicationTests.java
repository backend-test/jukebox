package com.octave.jukeboxspringboot;



import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.octave.jukeboxspringboot.controller.JukeBoxController;
import com.octave.jukeboxspringboot.dao.JukeBoxDaoImpl;
import com.octave.jukeboxspringboot.service.JukeBoxServiceImpl;

@SpringBootTest
class JukeboxSpringbootApplicationTests {
	
	@Autowired
	private JukeBoxController controller;
	
	@Autowired
	private JukeBoxServiceImpl service;
	
	@MockBean
	private JukeBoxDaoImpl dao;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
		assertThat(service).isNotNull();
		assertThat(dao).isNotNull();
	}

}
