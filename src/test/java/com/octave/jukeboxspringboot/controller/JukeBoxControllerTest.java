package com.octave.jukeboxspringboot.controller;

import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.octave.jukeboxspringboot.model.JukeBox;
import com.octave.jukeboxspringboot.model.JukeBoxComponent;
import com.octave.jukeboxspringboot.service.JukeBoxService;

@WebMvcTest(JukeBoxController.class)
public class JukeBoxControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private JukeBoxService service;

	@Test
	void getJukeBoxTest() throws Exception {
		JukeBox mockJk = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = singletonList(mockJk);
		
		Mockito.when(service.getJukeBox(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(mockJkList);
		mockMvc.perform(get("/jukebox?setting=qwe")).andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].id", is(mockJk.getId())))
        .andExpect(jsonPath("$[0].model", is(mockJk.getModel())));
	}
	
	@Test
	void getJukeBoxTest_SettingParamMissing() throws Exception {
		mockMvc.perform(get("/jukebox?ddsetting=qwe")).andDo(print())
		.andExpect(status().is4xxClientError());
	}
}
