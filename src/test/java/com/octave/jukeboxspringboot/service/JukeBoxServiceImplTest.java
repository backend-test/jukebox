package com.octave.jukeboxspringboot.service;

import static java.util.Collections.singletonList;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.octave.jukeboxspringboot.dao.JukeBoxDao;
import com.octave.jukeboxspringboot.exception.ServiceException;
import com.octave.jukeboxspringboot.model.JukeBox;
import com.octave.jukeboxspringboot.model.JukeBoxComponent;
import com.octave.jukeboxspringboot.model.Setting;

@SpringBootTest
public class JukeBoxServiceImplTest {

	@Autowired
	private JukeBoxService service;
	
	@MockBean
	private JukeBoxDao dao;
	
	@Test
	void getJukeBoxTest_SettingValid() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = singletonList(mockJk);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "", -1, -1);
		Assert.assertEquals(1, response.size());
		Assert.assertEquals(mockJkList, response);
	}
	
	@Test
	void getJukeBoxTest_SettingInValid() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = singletonList(mockJk);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		try {
			service.getJukeBox("s2", "", -1, -1);
		} catch (Exception e) {
			String error = "Invalid Setting Id::" + "s2";
			Assert.assertTrue(e instanceof ServiceException);
			Assert.assertEquals(error, e.getMessage());
		}
		
	}
	
	@Test
	void getJukeBoxTest_SettingValid_ModelExists() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = singletonList(mockJk);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "fusion", -1, -1);
		Assert.assertEquals(1, response.size());
		Assert.assertEquals(mockJkList, response);
	}
	
	@Test
	void getJukeBoxTest_SettingValid_ModelNotExists() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = singletonList(mockJk);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "angelina", -1, -1);
		Assert.assertTrue(response.isEmpty());
	}
	
	@Test
	void getJukeBoxTest_ModelNotExistsForSetting() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk1 = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		JukeBox mockJk2 = new JukeBox("jk2", "angelina", 
				new JukeBoxComponent[] {new JukeBoxComponent("pcb")});
		List<JukeBox> mockJkList = new ArrayList<JukeBox>();
		mockJkList.add(mockJk1);
		mockJkList.add(mockJk2);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "angelina", 1, -1);
		Assert.assertTrue(response.isEmpty());
	}
	
	@Test
	void getJukeBoxTest_Juke1Offset1() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = singletonList(mockJk);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "fusion", 1, -1);
		Assert.assertTrue(response.isEmpty());
	}
	
	@Test
	void getJukeBoxTest_Juke2Offset1() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk1 = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		JukeBox mockJk2 = new JukeBox("jk2", "angelina", 
				new JukeBoxComponent[] {new JukeBoxComponent("pcb"), new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = new ArrayList<JukeBox>();
		mockJkList.add(mockJk1);
		mockJkList.add(mockJk2);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "", 1, -1);
		Assert.assertEquals(1, response.size());
		Assert.assertEquals(mockJk2, response.get(0));
	}
	
	@Test
	void getJukeBoxTest_OffsetMoreThanElements() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = singletonList(mockJk);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "fusion", 3, -1);
		Assert.assertTrue(response.isEmpty());
	}
	
	@Test
	void getJukeBoxTest_Limit0() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = singletonList(mockJk);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "fusion", -1, 0);
		Assert.assertTrue(response.isEmpty());
	}
	
	@Test
	void getJukeBoxTest_Juke2Limit1() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk1 = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		JukeBox mockJk2 = new JukeBox("jk2", "angelina", 
				new JukeBoxComponent[] {new JukeBoxComponent("pcb")});
		List<JukeBox> mockJkList = new ArrayList<JukeBox>();
		mockJkList.add(mockJk1);
		mockJkList.add(mockJk2);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "", -1, 1);
		Assert.assertEquals(1, response.size());
		Assert.assertEquals(mockJk1, response.get(0));
	}
	

	
	@Test
	void getJukeBoxTest_LimitMoreThanElements() {
		Setting mockSet = new Setting("s1",
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		Mockito.when(dao.fetchSetting(Mockito.anyString())).thenReturn(mockSet);
		
		JukeBox mockJk1 = new JukeBox("jk1", "fusion", 
				new JukeBoxComponent[] {new JukeBoxComponent("money_receiver")});
		JukeBox mockJk2 = new JukeBox("jk2", "angelina", 
				new JukeBoxComponent[] {new JukeBoxComponent("pcb"), new JukeBoxComponent("pcb"), new JukeBoxComponent("money_receiver")});
		List<JukeBox> mockJkList = new ArrayList<JukeBox>();
		mockJkList.add(mockJk1);
		mockJkList.add(mockJk2);
		Mockito.when(dao.fetchAllJukeBox()).thenReturn(mockJkList);
		
		List<JukeBox> response = service.getJukeBox("s1", "", -1, 5);
		Assert.assertEquals(2, response.size());
		Assert.assertTrue(response.containsAll(mockJkList));
	}
	
}
